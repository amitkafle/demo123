package com.example.mynewapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    private Button one;
    private Button two;
    private Button three;
    private Button four;
    private Button five;
    private Button six;
    private Button seven;
    private Button eight;
    private Button nine;
    private Button add;
    private Button sub;
    private Button mul;
    private Button div;
    private Button clear;
    private Button deci;
    private Button equals;
    private Button zero;
    private TextView textViewinput;
    private TextView textViewresult;
    private final String ADDITION = "+";
    private final String SUBTRACTION = "-";
    private final String MULTIPLICATION = "*";
    private final String DIVISION = "/";
    private final String EQU = "0";
    private double val1 = Double.NaN;
    private double val2;
    private String ACTION;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupUIViews();

        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewinput.setText(textViewinput.getText().toString() + "0");
            }
        });
        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewinput.setText(textViewinput.getText().toString() + "1");

            }
        });
        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewinput.setText(textViewinput.getText().toString() + "2");
            }
        });
        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewinput.setText(textViewinput.getText().toString() + "3");
            }
        });
        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewinput.setText(textViewinput.getText().toString() + "4");
            }
        });
        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewinput.setText(textViewinput.getText().toString() + "5");
            }
        });
        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewinput.setText(textViewinput.getText().toString() + "6");
            }
        });
        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewinput.setText(textViewinput.getText().toString() + "7");
            }
        });
        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewinput.setText(textViewinput.getText().toString() + "8");
            }
        });
        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewinput.setText(textViewinput.getText().toString() + "9");
            }
        });

        mul.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                compute();
                ACTION = MULTIPLICATION;
                textViewresult.setText(String.valueOf(val1) + "*");
                textViewinput.setText(null);
            }
        });
        div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compute();
                ACTION = DIVISION;
                textViewresult.setText(String.valueOf(val1) + "/");
                textViewinput.setText(null);
            }
        });
        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compute();
                ACTION = SUBTRACTION;
                textViewresult.setText(String.valueOf(val1) + "-");
                textViewinput.setText(null);
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compute();
                ACTION = ADDITION;
                textViewresult.setText(String.valueOf(val1) + "+");
                textViewinput.setText(null);
            }
        });
        equals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                compute();
                ACTION = EQU;
                textViewresult.setText(textViewresult.getText().toString() + String.valueOf(val2) + "=" + String.valueOf(val1));
                textViewinput.setText(null);
            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textViewinput.getText().length() > 0) {
                    CharSequence name = textViewinput.getText().toString();
                    textViewinput.setText(name.subSequence(0, name.length() - 1));

                } else {
                    val1 = Double.NaN;
                    val2 = Double.NaN;
                    textViewinput.setText(null);
                    textViewresult.setText(null);

                }

            }
        });
    }




    private void setupUIViews(){
        one = (Button) findViewById(R.id.btn1);
        zero = (Button)findViewById(R.id.btn0);
        two = (Button) findViewById(R.id.btn2);
        three = (Button)findViewById(R.id.btn3);
        four = (Button) findViewById(R.id.btn4);
        five = (Button)findViewById(R.id.btn5);
        six = (Button) findViewById(R.id.btn6);
        seven = (Button)findViewById(R.id.btn7);
        eight = (Button) findViewById(R.id.btn8);
        nine = (Button)findViewById(R.id.btn9);
        mul = (Button) findViewById(R.id.btnmultiply);
        add = (Button)findViewById(R.id.btnadd);
        sub = (Button) findViewById(R.id.btnsubtract);
        div = (Button)findViewById(R.id.btndivide);
        equals = (Button)findViewById(R.id.btnequals);
        deci = (Button) findViewById(R.id.btndeci);
        clear = (Button) findViewById(R.id.btnclear);
        textViewresult = (TextView)findViewById(R.id.textViewinput);
        textViewinput = (TextView)findViewById(R.id.textViewinput);
    }
    private void compute() {
        if (!Double.isNaN(val1)) {
            val2 = Double.parseDouble(textViewinput.getText().toString());
            switch (ACTION) {
                case ADDITION:
                    val1 = val1 + val2;
                    break;
                case SUBTRACTION:
                    val1 = val1 - val2;
                    break;
                case MULTIPLICATION:
                    val1 = val1 * val2;
                    break;
                case DIVISION:
                    val1 = val1 / val2;
                    break;
                case EQU:
                    break;
            }
        }
        else{
            val1 = Double.parseDouble(textViewinput.getText().toString());

        }
    }
}

